package net.pl3x.pl3xconsolefilter.commands;

import net.pl3x.pl3xconsolefilter.Pl3xConsoleFilter;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CmdPl3xConsoleFilter implements CommandExecutor {
	private Pl3xConsoleFilter plugin;
	
	public CmdPl3xConsoleFilter(Pl3xConsoleFilter plugin) {
		this.plugin = plugin;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(!cmd.getName().equalsIgnoreCase("pl3xconsolefilter"))
			return false;
		if (sender instanceof Player) {
			sender.sendMessage(plugin.colorize("&4This command is a console-only command."));
			return true;
		}
		if (args.length > 0) {
			if (args[0].equalsIgnoreCase("reload")) {
				plugin.reloadConfig();
				plugin.reloadFilter();
				sender.sendMessage(plugin.colorize("&d" + plugin.getName() + " config.yml has been reloaded."));
				if(plugin.getConfig().getBoolean("debug-mode", false))
					plugin.log(plugin.colorize("&aReloaded config.yml"));
				return true;
			}
		}
		sender.sendMessage(plugin.colorize("&d" + plugin.getName() + " v" + plugin.getDescription().getVersion()));
		return true;
	}
}
